﻿using ProjectSEM3.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSEM3.Models
{
    public class Book
    {
        public Book()
        {
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = DateTime.Now;
            this.Status = BookStatus.Active;
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Snippet { get; set; }

        public string Thumbnail { get; set; }

        public int Price { get; set; }

        public string Cover { get; set; }

        public string Overview { get; set; }

        public DateTime Published { get; set; }

        public int Hardcover { get; set; }

        public string Language { get; set; }

        public DateTime Dimensions { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public BookStatus Status { get; set; }

        public int Total { get; set; }

        [ForeignKey("Author")]
        public int AuthorId { get; set; }

        public virtual Author Author { get; set; }

        [ForeignKey("Account")]
        public int AccountId { get; set; }

        public virtual Account Account { get; set; }

        public List<BookCategory> BookCategory { get; set; }

    }

    public enum BookStatus
    {
        OutOfStock = 2,
        Active = 1,
        Deactive = 0
    }
    
}
