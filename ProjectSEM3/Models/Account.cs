﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSEM3.Models
{
    public class Account
    {
        public Account()
        {
            this.Avatar = "http://svgur.com/i/65U.svg";
            this.BirthDay = DateTime.Now;
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = DateTime.Now;
            this.Gender = AccountGender.Other;
            this.Role = AccountRole.Customer;
            this.Status = AccountStatus.Active;
        }

        [Key]
        public int Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Salt { get; set; }

        public string FullName { get; set; }

        public string Avatar { get; set; }

        public DateTime BirthDay { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public AccountGender Gender { get; set; }

        public AccountRole Role { get; set; }

        public AccountStatus Status { get; set; }


        private static Random random = new Random();

        public static string RandomString(int length)
        {
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }

    public enum AccountStatus
    {
        Active = 1,
        Deactive = 2
    }

    public enum AccountGender
    {
        Female = 0,
        Male = 1,
        Other = 2
    }

    public enum AccountRole
    {
        Admin = 1,
        Customer = 0
    }
}
