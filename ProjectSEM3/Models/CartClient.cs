﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSEM3.Models
{
    public class CartClient
    {
        public CartClient()
        {

        }

        [Key]
        public int Id { get; set; }

        public string Thumbnail { get; set; }

        public string Name { get; set; }

        public int Price { get; set; }

        public int Quantity { get; set; }

        public int Total { get; set; }
    }
}
