﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSEM3.Models
{
    public class Author
    {
        public Author()
        {
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = DateTime.Now;
            this.Status = AuthorStatus.Active;
        }
        [Key]
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Quote { get; set; }
        public string Avatar { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public AuthorStatus Status { get; set; }
        public virtual ICollection<Book> Books { get; set; }
    }

    public enum AuthorStatus
    {
        Active = 1,
        Deactive = 0
    }
}
