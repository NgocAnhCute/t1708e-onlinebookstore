﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSEM3.Models
{
    public class Dashboard
    {
        public int account_count { get; set; }
        public int author_count { get; set; }
        public int book_count { get; set; }
        public int order_count { get; set; }
    }
}
