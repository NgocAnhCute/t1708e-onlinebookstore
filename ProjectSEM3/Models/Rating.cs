﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSEM3.Models
{
    public class Rating
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Account")]
        public int OwnerId { get; set; }

        [ForeignKey("Book")]
        public int BookId { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}
