﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSEM3.Models
{
    public class PaypalLogger
    {
        public static readonly string LogDirectioryPath = AppDomain.CurrentDomain.BaseDirectory;

        public static void Log(string messages)
        {
            try
            {
                StreamWriter strw = new StreamWriter(LogDirectioryPath + "\\PaypalError.log", true);
                Debug.WriteLine(LogDirectioryPath);
                strw.WriteLine("{0} ---> {1}", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), messages);
                strw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }

}
