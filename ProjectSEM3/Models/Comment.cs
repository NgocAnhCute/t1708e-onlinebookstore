﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSEM3.Models
{
    public class Comment
    {
        public Comment()
        {
            this.Status = CommentStatus.Show;
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        [ForeignKey("Book")]
        public int BookId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Comments { get; set; }

        [ForeignKey("Rating")]
        public float RatingId { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public CommentStatus Status { get; set; }
    }

    public enum CommentStatus
    {
        Hide = 0,
        Show = 1
    }
}
