﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSEM3.Models
{
    public class BookCategory
    {
        [Key]
        public int Id { get; set; }
        
        public int BookId { get; set; }
        
        public int CategoryId { get; set; }
    }
}
