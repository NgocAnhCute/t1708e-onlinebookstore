﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSEM3.Models
{
    public class Category
    {
        public Category()
        {
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = DateTime.Now;
            this.Status = CategoryStatus.Active;
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public CategoryStatus Status { get; set; }

        public List<BookCategory> BookCategory { get; set; }
    }

    public enum CategoryStatus
    {
        Active = 1,
        Deactive = 0
    }
}
