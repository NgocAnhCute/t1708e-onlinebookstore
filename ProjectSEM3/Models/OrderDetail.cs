﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSEM3.Models
{
    public class OrderDetail
    {
        public OrderDetail()
        {
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        public int OrderId { get; set; }

        public virtual OrderBook OrderBook { get; set; }

        [ForeignKey("Book")]
        public int BookId { get; set; }

        public virtual Book Book { get; set; }

        public int Quantity { get; set; }

        public OrderDetailStatus Status { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

    }

    public enum OrderDetailStatus
    {
        Active = 1,
        Deactive = 2
    }
}
