﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MlkPwgen;
using Newtonsoft.Json;
using ProjectSEM3.Data;
using ProjectSEM3.Models;
using SecurityHelper;

namespace ProjectSEM3.Controllers
{
    public class AuthController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;

        public AuthController(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;


        }
        public IActionResult Index()
        {
            return View();
        }

        [Route("Login")]
        [Route("Auth/Login")]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SignIn([Bind("Username,Password")] Account account)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var existAccount = _context.Account.SingleOrDefault(a => a.Username == account.Username);
            if (existAccount != null)
            {
                if (existAccount.Password == PasswordHandle.GetInstance().EncryptPassword(account.Password, existAccount.Salt))
                {
                    var existCredential = await _context.Credential.SingleOrDefaultAsync(c =>
                            c.OwnerId == existAccount.Id);
                    if (existCredential != null)
                    {
                        var accessToken = PasswordGenerator.Generate(length: 40, allowed: Sets.Alphanumerics);
                        existCredential.AccessToken = accessToken;
                        await _context.SaveChangesAsync();
                        _session.SetString("auth", existCredential.AccessToken);
                    }
                    else
                    {
                        var credential = new Credential(existAccount.Id)
                        {
                            AccessToken = PasswordGenerator.Generate(length: 40, allowed: Sets.Alphanumerics)
                        };
                        _context.Credential.Add(credential);
                        await _context.SaveChangesAsync();
                        _session.SetString("auth", credential.AccessToken);

                    }
                    _session.SetString("user", JsonConvert.SerializeObject(existAccount));

                    if (existAccount.Role == AccountRole.Admin)
                    {
                        return RedirectToAction("Dashboard", "Admin");
                    }
                    else if (existAccount.Role == AccountRole.Customer)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                TempData["ErrorMessage"] = "Mật khẩu không chính xác!";
                return RedirectToAction(nameof(Login));
            }
            TempData["ErrorMessage"] = "Username không tồn tại";
            return RedirectToAction(nameof(Login));
        }

        [Route("Register")]
        [Route("Auth/Register")]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> RegisterPost(Account account)
        {
            if (ModelState.IsValid)
            {
                if (_context.Account.Any(e => e.Username == account.Username))
                {
                    TempData["ErrorMessage"] = "Username have exists";
                    return RedirectToAction(nameof(Register));
                }
                var salt = PasswordHandle.GetInstance().GenerateSalt();
                var passwordEncode = PasswordHandle.GetInstance().EncryptPassword(account.Password, salt);
                account.Salt = salt;
                account.Password = passwordEncode;

                _context.Add(account);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Login));
            }
            return RedirectToAction(nameof(Login));
        }

        [Route("Logout")]
        [Route("Auth/Logout")]
        public IActionResult Logout()
        {
            _session.SetString("user", "");
            _session.SetString("auth", "");
            _session.Remove("user");
            _session.Remove("auth");
            return RedirectToAction(nameof(Login));
        }


    }
}