﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MlkPwgen;
using Newtonsoft.Json;
using ProjectSEM3.Data;
using ProjectSEM3.Models;
using SecurityHelper;

namespace ProjectSEM3.Controllers
{
    public class AdminController : Controller
    {
        
        private readonly ApplicationDbContext _context;

        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;

        public AdminController(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            

        }

        [Route("Admin/User")]
        public IActionResult Account()
        {
            return View();
        }

        public bool CheckLoginAdmin()
        {
            var isAuthen = false;
            var accessToken = _session.GetString("auth");
            Debug.WriteLine(accessToken);
            if (accessToken != null)
            {
                var existCredential = _context.Credential.SingleOrDefault(a => a.AccessToken == accessToken);
                Debug.WriteLine(existCredential.OwnerId);
                if (existCredential != null)
                {
                    var account = _context.Account.SingleOrDefault(a => a.Id == existCredential.OwnerId);
                    //ViewData["imageUser"] = account.Avatar;
                    //ViewData["fullnameUser"] = account.FullName;
                    Debug.WriteLine(account.FullName);
                    isAuthen = true;
                }
            }
            return isAuthen;
        }

        [Route("Admin")]
        [Route("Admin/Dashboard")]
        public IActionResult Dashboard()
        {
            Dashboard dashboard = new Dashboard();
            dashboard.account_count = _context.Account.Count();
            dashboard.author_count = _context.Author.Count();
            dashboard.book_count = _context.Book.Count();
            dashboard.order_count = _context.Order.Count();

            var accessToken = _session.GetString("auth");
            Debug.WriteLine(accessToken);
            if (accessToken != null)
            {
                var existCredential = _context.Credential.SingleOrDefault(a => a.AccessToken == accessToken);
                Debug.WriteLine(existCredential.OwnerId);
                if (existCredential != null)
                {
                    var account = _context.Account.SingleOrDefault(a => a.Id == existCredential.OwnerId);
                    ViewData["imageUser"] = account.Avatar;
                    ViewData["fullnameUser"] = account.FullName;
                    Debug.WriteLine(account.FullName);
                }
                else
                {
                    return RedirectToAction(nameof(LoginAdmin));
                }
            }
            else
            {
                return RedirectToAction(nameof(LoginAdmin));
            }
            

            return View(dashboard);
        }

        //private Credential GetObjectFromJson<Credential>(this ISession session, string key)
        //{
        //    var value = session.GetString(key);

        //    return value == null ? default(Credential) : JsonConvert.DeserializeObject<Credential>(value);
        //}

        [Route("Admin/Profile")]
        public IActionResult Profile()
        {
            return View();
        }

        // GET: Admin
        
        [Route("Admin/Admin")]
        public async Task<IActionResult> Index()
        {
            var accessToken = _session.GetString("auth");
            Debug.WriteLine(accessToken);
            if (accessToken != null)
            {
                var existCredential = _context.Credential.SingleOrDefault(a => a.AccessToken == accessToken);
                Debug.WriteLine(existCredential.OwnerId);
                if (existCredential != null)
                {
                    var account = _context.Account.SingleOrDefault(a => a.Id == existCredential.OwnerId);
                    ViewData["imageUser"] = account.Avatar;
                    ViewData["fullnameUser"] = account.FullName;
                    Debug.WriteLine(account.FullName);
                }
            }
            return View(await _context.Account.ToListAsync());
        }

        // GET: Admin/Details/5
        [Route("Admin/Account/Details")]
        public async Task<IActionResult> Details(float? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var account = await _context.Account
                .FirstOrDefaultAsync(m => m.Id == id);
            if (account == null)
            {
                return NotFound();
            }

            return View(account);
        }

        // GET: Admin/Create
        [Route("Admin/Account/Create")]
        public IActionResult Create()
        {
            return View();
        }
        [Route("Admin/LoginAdmin")]
        public IActionResult LoginAdmin()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SignIn([Bind("Username,Password")] Account account)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var existAccount = _context.Account.SingleOrDefault(a => a.Username == account.Username);
            if (existAccount != null)
            {
                if (existAccount.Password == PasswordHandle.GetInstance().EncryptPassword(account.Password, existAccount.Salt))
                {
                    var existCredential = await _context.Credential.SingleOrDefaultAsync(c =>
                            c.OwnerId == existAccount.Id);
                    if (existCredential != null)
                    {
                        var accessToken = PasswordGenerator.Generate(length: 40, allowed: Sets.Alphanumerics);
                        existCredential.AccessToken = accessToken;
                        await _context.SaveChangesAsync();
                        _session.SetString("auth", existCredential.AccessToken);
                    }
                    else
                    {
                        var credential = new Credential(existAccount.Id)
                        {
                            AccessToken = PasswordGenerator.Generate(length: 40, allowed: Sets.Alphanumerics)
                        };
                        _context.Credential.Add(credential);
                        await _context.SaveChangesAsync();
                        _session.SetString("auth", credential.AccessToken);

                    }
                    _session.SetString("user", JsonConvert.SerializeObject(existAccount));
                    return RedirectToAction(nameof(Index));
                }
                TempData["ErrorMessage"] = "Mật khẩu không chính xác!";
                return RedirectToAction(nameof(LoginAdmin));
            }
            TempData["ErrorMessage"] = "Username không tồn tại";
            return RedirectToAction(nameof(LoginAdmin));
        }

        [Route("Admin/Register")]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> RegisterPost(Account account)
        {
            if (ModelState.IsValid)
            {
                Debug.WriteLine("---------------------------------------------");
                Debug.WriteLine(account.Password);
                if (AccountExistsUsername(account.Username))
                {
                    TempData["ErrorMessage"] = "Username have exists";
                    return RedirectToAction(nameof(Register));
                }
                var salt = PasswordHandle.GetInstance().GenerateSalt();
                Debug.WriteLine(salt);
                var passwordEncode = PasswordHandle.GetInstance().EncryptPassword(account.Password, salt);
                Debug.WriteLine(passwordEncode);
                account.Salt = salt;
                account.Password = passwordEncode;
                Debug.WriteLine(account.Password);

                _context.Add(account);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(LoginAdmin));
            }
            return RedirectToAction(nameof(LoginAdmin));
        }

        private bool AccountExistsUsername(string username)
        {
            return _context.Account.Any(e => e.Username == username);
        }

        // POST: Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Username,Password,Status")] Account account)
        {
            if (ModelState.IsValid)
            {
                _context.Add(account);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(account);
        }

        // GET: Admin/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var account = await _context.Account.FindAsync(id);
            if (account == null)
            {
                return NotFound();
            }
            return View(account);
        }

        // POST: Admin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Username,Password,Status")] Account account)
        {
            if (id != account.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(account);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AccountExists(account.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(account);
        }

        // GET: Admin/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var account = await _context.Account
                .FirstOrDefaultAsync(m => m.Id == id);
            if (account == null)
            {
                return NotFound();
            }

            return View(account);
        }

        // POST: Admin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var account = await _context.Account.FindAsync(id);
            _context.Account.Remove(account);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AccountExists(int id)
        {
            return _context.Account.Any(e => e.Id == id);
        }
    }
}
