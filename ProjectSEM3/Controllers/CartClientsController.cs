﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectSEM3.Data;
using ProjectSEM3.Models;


using System.Linq;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;

namespace ProjectSEM3.Controllers
{
    [Route("api/order")]
    [ApiController]
    public class CartClientsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public CartClientsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/CartClients
        [HttpGet]
        public IEnumerable<CartClient> GetCartClient()
        {
            return _context.CartClient;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        [HttpPost]
        [Route("paypal")]
        public async Task<IActionResult> PaypalResponse([FromQuery] string orderId)
        {
            var PAYPAL_CLIENT = "AReAshAU5jrxQ6WXRVQG9-IaDU-5Pl8SwDCrUMnKL1PVtAjbdgTiO1or1Dm3V-SDIsS1eHUt8fCj7LTO";
            var PAYPAL_SECRET = "EDmLmrfQjMeBr9gyV_zN9X3TGtqnwdCGOBrzPnucnIRvt8EycyM9vt7zWVAKkgIyhs2iAeKxjFXLqrVa";
            
            var PAYPAL_OAUTH_API = "https://api.sandbox.paypal.com/v1/oauth2/token";
            var PAYPAL_ORDER_API = "https://api.sandbox.paypal.com/v2/checkout/orders/";

            var plainTextBytes = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(PAYPAL_CLIENT + ":" + PAYPAL_SECRET));
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + plainTextBytes);
            var dict = new Dictionary<string, string>
            {
                { "grant_type", "client_credentials" }
            };

            var resultAuth = await client.PostAsync(PAYPAL_OAUTH_API, new FormUrlEncodedContent(dict));
            
            var paypalAccess = JsonConvert.DeserializeObject<dynamic>(resultAuth.Content.ReadAsStringAsync().Result);
            
            HttpClient clientOrder = new HttpClient();

            clientOrder.DefaultRequestHeaders.Add("Authorization", "Bearer " + paypalAccess.access_token);

            var OrderInfo = clientOrder.GetAsync(PAYPAL_ORDER_API + orderId).Result.Content.ReadAsStringAsync().Result;
            var a = JObject.Parse(OrderInfo);

            if (a["status"].ToString() == "COMPLETED" || a["status"].ToString() == "APPROVED")
            {
                Response.StatusCode = (int)HttpStatusCode.OK;
                return Ok("Payment success!");
            }
            Response.StatusCode = (int)HttpStatusCode.Forbidden;
            return new JsonResult(a);
        }

        // PUT: api/CartClients/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCartClient([FromRoute] int id, [FromBody] CartClient cartClient)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cartClient.Id)
            {
                return BadRequest();
            }

            _context.Entry(cartClient).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CartClientExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CartClients
        [HttpPost]
        public async Task<IActionResult> PostCartClient([FromBody] CartClient cartClient)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.CartClient.Add(cartClient);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCartClient", new { id = cartClient.Id }, cartClient);
        }

        

        // DELETE: api/CartClients/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCartClient([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cartClient = await _context.CartClient.FindAsync(id);
            if (cartClient == null)
            {
                return NotFound();
            }

            _context.CartClient.Remove(cartClient);
            await _context.SaveChangesAsync();

            return Ok(cartClient);
        }

        private bool CartClientExists(int id)
        {
            return _context.CartClient.Any(e => e.Id == id);
        }
    }
}