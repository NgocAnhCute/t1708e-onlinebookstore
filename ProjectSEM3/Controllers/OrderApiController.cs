﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectSEM3.Data;
using ProjectSEM3.Models;

namespace ProjectSEM3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderApiController : ControllerBase
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;

        private readonly ApplicationDbContext _context;

        public OrderApiController(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;


        }

        // GET: api/OrderApi
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/OrderApi/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/OrderApi
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] List<CartClient> listCart)
        {
            
            var order = new OrderBook()
            {
                TotalPrice = 0,
                AccountId = getUserAuth().Id

            };
            _context.Add(order);
            await _context.SaveChangesAsync();
            var orderId = order.Id;
            var totalPrice = 0;
            foreach (CartClient cart in listCart)
            {
                var orderDetail = new OrderDetail() {
                    OrderId = orderId,
                    BookId = cart.Id,
                    Quantity = cart.Quantity
                };
                totalPrice += cart.Price;
                _context.Add(orderDetail);
                await _context.SaveChangesAsync();

            }
            order.TotalPrice = totalPrice;
            _context.Update(order);

            return Ok(order);
        }

        // PUT: api/OrderApi/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        public Account getUserAuth()
        {
            var userStr = _session.GetString("user");
            return JsonConvert.DeserializeObject<Account>(userStr);
        }
    }
}
