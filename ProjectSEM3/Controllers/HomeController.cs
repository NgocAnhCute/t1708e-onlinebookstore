﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PayPal.Api;
using Microsoft.AspNetCore.Http;
using ProjectSEM3.Data;
using ProjectSEM3.Models;
using Newtonsoft.Json;

namespace ProjectSEM3.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;

        public HomeController(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }
        public IActionResult Account()
        {
            var userStr = _session.GetString("user");
            if (userStr == null)
            {
                return RedirectToAction(nameof(Index));
            }
            getDataShare();
            
            return View();
        }

        [Route("")]
        [Route("Home")]
        [Route("Home/Index")]
        public async Task<IActionResult> Index()
        {
            var authors = await _context.Author.ToListAsync();
            var newestRelease = _context.Book.OrderByDescending(a => a.CreatedAt).Take(4).ToList();
            ViewData["NewestRelease"] = getBook(newestRelease, authors);

            var recommentdedForYou = _context.Book.OrderBy(a => a.Price).Take(5).ToList();
            ViewData["RecommentdedForYou"] = getBook(recommentdedForYou, authors);

            ViewData["Author"] = _context.Author.Take(5).ToList();

            var books = await _context.Book.Take(12).ToListAsync();
            ViewData["Books"] = getBook(books, authors);

            getDataShare();
            return View();
        }

        private List<Book> getBook(List<Book> books, List<Author> authors)
        {
            foreach (Book book in books)
            {
                foreach (Author author in authors)
                {
                    if (author.Id == book.AuthorId)
                    {
                        book.Author = author;
                    }
                }
            }
            return books;
        }

        [Route("Home/Detail")]
        public IActionResult Detail([FromQuery(Name = "id")] string id)
        {
            if (id == null || !id.ToString().All(char.IsDigit))
            {
                return RedirectToAction("Index");
            }

            var book = _context.Book.SingleOrDefault(b => b.Id.ToString() == id);
            book.Author = _context.Author.Find(book.AuthorId);
            if (book != null)
            {
                ViewData["Title"] = book.Name;
                getDataShare();
                return View(book);
            }
            else
            {
                return NotFound();
            }
        }

        public IActionResult Contact()
        {
            getDataShare();
            return View();
        }

        public IActionResult Cart()
        {
            getDataShare();

            return View();
        }

        public IActionResult Categories()
        {
            getDataShare();

            return View();
        }

        public IActionResult Checkout()
        {
            getDataShare();

            return View();
        }
        public IActionResult FAQ()
        {
            getDataShare();

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult Orderdetail()
        {

            getDataShare();
            return View();
        }

        public async Task<IActionResult> Search([FromQuery] string value, [FromQuery] int categoryId, [FromQuery] int min_price, [FromQuery] int max_price, [FromQuery] string categories)
        {
            Debug.WriteLine("======"+categories);
            getDataShare();
            var bookIds = _context.BookCategory.Where(c => c.CategoryId == categoryId).Select(l => l.BookId).ToArray();
            if (categoryId == null || categoryId == 0)
            {
                bookIds = _context.Book.Select(l => l.Id).ToArray();
            }
            
            if (String.IsNullOrEmpty(value)) {
                value = " ";
            }
            if (min_price == null)
            {
                min_price = 0;
            }
            if (max_price == null)
            {
                max_price = 100;
            }
            var books = await _context.Book
                .Where(b => b.Name.Contains(value) || b.Overview.Contains(value))
                .Where(b => bookIds.Contains(b.Id))
                .Where(b => b.Price >= min_price && b.Price <= max_price)
                .ToListAsync();
            Debug.WriteLine(books);
            var authors = await _context.Author.ToListAsync();

            foreach (Book book in books)
            {
                foreach(Author author in authors)
                {
                    if(author.Id == book.AuthorId)
                    {
                        book.Author = author;
                    }
                }
            }
            ViewData["searchBook"] = books;
            ViewData["valueSearch"] = value;
            ViewData["countSearch"] = books.Count();
            return View();
        }
        public IActionResult AdminLogin()
        {
            return View();
        }

        [Route("Home/Author")]
        public IActionResult Author([FromQuery(Name = "id")] string id)
        {
            if (id == null || !id.ToString().All(char.IsDigit))
            {
                return RedirectToAction("Index");
            }

            var author = _context.Author.SingleOrDefault(b => b.Id.ToString() == id);
            if (author != null)
            {
                ViewData["Title"] = author.FullName;
                getDataShare();
                return View(author);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public string SearchPost(string searchString)
        {
            return "From [HttpPost]Index: filter on " + searchString;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public void getDataShare()
        {
            ViewData["ListCategories"] = _context.Category.ToList();
            ViewData["ThisWeekReviewd"] = _context.Book.Take(3).ToList();
            //get userAuth
            var userStr = _session.GetString("user");
            if (userStr != null)
            {
                var user = JsonConvert.DeserializeObject<Account>(userStr);
                ViewData["userAuth"] = user;
            }
        }
    }
}
