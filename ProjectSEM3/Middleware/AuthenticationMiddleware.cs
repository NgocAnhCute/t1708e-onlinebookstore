﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectSEM3.Models;

namespace ProjectSEM3.Middleware
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class AuthenticationMiddleware
    {
        //private readonly ApplicationDbContext dbContext;
        private readonly RequestDelegate _next;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;

        public AuthenticationMiddleware(RequestDelegate next, IHttpContextAccessor httpContextAccessor)
        {
            //dbContext = _dbContext;
            _next = next;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task Invoke(HttpContext context)
        {
            var userStr = _session.GetString("user");
            var accessToken = _session.GetString("auth");

            var protectedUrl = new List<string>
            {
                "/admin",
                "/account",
                "/authors/",
                "/book/",
                "/categories/",
                "/order/",
                "/home/account"
            };
            
            if (protectedUrl.Any(context.Request.Path.ToString().ToLower().Contains)) {
                if (userStr != null && accessToken != null)
                {
                    var user = JsonConvert.DeserializeObject<Account>(userStr);
                    if (user.Role == AccountRole.Admin)
                    {
                        await _next(context);
                    }
                    else if (user.Role == AccountRole.Customer)
                    {
                        await _next(context);
                    }
                }
                context.Response.Redirect("/Auth/Login");
            }

            await _next(context);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class AuthenticationMiddlewareExtensions
    {
        public static IApplicationBuilder UseAuthenticationMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthenticationMiddleware>();
        }
    }
}
