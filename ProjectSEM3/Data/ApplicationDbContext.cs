﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ProjectSEM3.Models;

namespace ProjectSEM3.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Account> Account { get; set; }
        public DbSet<Book> Book { get; set; }
        public DbSet<Author> Author { get; set; }
        public DbSet<Credential> Credential { get; set; }
        public DbSet<BookCategory> BookCategory { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Comment> Comment { get; set; }
        public DbSet<Rating> Rating { get; set; }
        public DbSet<Cart> Cart { get; set; }
        public DbSet<OrderBook> Order { get; set; }
        public DbSet<OrderDetail> OrderDetail { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Account>().HasData(new Account() {
                Id = 1,
                Username = "Quyến",
                Password = "CE08F99591F72ED0C07DCFC4EB15BE7E",
                Salt = "934c5eb2-b364-41c1-a9d0-34b8ee0fe435",
                FullName = "Quyến đỗ",
                Role = AccountRole.Admin
            }, new Account()
            {
                Id = 2,
                Username = "Tuấn",
                //9876543210
                Password = "CE08F99591F72ED0C07DCFC4EB15BE7E",
                Salt = "934c5eb2-b364-41c1-a9d0-34b8ee0fe435",
                FullName = "Tuấn Ngô",
                Role = AccountRole.Admin
            });

            modelBuilder.Entity<Author>().HasData(new Author()
            {
                Id = 1,
                FullName = "Amy Brecount White",
                Quote = "Nunc sed lacinia odio, eget ultrices ante. Aliquam tincidunt, nisl at iaculis pulvinar, dolor nisl posuere est, consequat aliquam ante diam non sapien. Ut a venenatis diam. Curabitur sed turpis accumsan, vestibulum nunc imperdiet, condimentum purus. Donec a nunc vel libero congue semper consectetur et lorem",
                Avatar = "http://demo.cmssuperheroes.com/themeforest/bookjunky/wp-content/uploads/lady_author_3.jpg"
            }, new Author()
            {
                Id = 2,
                FullName = "Andrew Barkley & Paul W. Barkley",
                Quote = "Nunc sed lacinia odio, eget ultrices ante. Aliquam tincidunt, nisl at iaculis pulvinar, dolor nisl posuere est, consequat aliquam ante diam non sapien. Ut a venenatis diam. Curabitur sed turpis accumsan, vestibulum nunc imperdiet, condimentum purus. Donec a nunc vel libero congue semper consectetur et lorem",
                Avatar = "http://demo.cmssuperheroes.com/themeforest/bookjunky/wp-content/uploads/man_author_2.jpg"
            }, new Author()
            {
                Id = 3,
                FullName = "Andrea Cremer",
                Quote = "Nunc sed lacinia odio, eget ultrices ante. Aliquam tincidunt, nisl at iaculis pulvinar, dolor nisl posuere est, consequat aliquam ante diam non sapien. Ut a venenatis diam. Curabitur sed turpis accumsan, vestibulum nunc imperdiet, condimentum purus. Donec a nunc vel libero congue semper consectetur et lorem.",
                Avatar = "http://demo.cmssuperheroes.com/themeforest/bookjunky/wp-content/uploads/lady_author.jpg"
            }, new Author()
            {
                Id = 4,
                FullName = "Aprilynne Pike",
                Quote = "Nunc sed lacinia odio, eget ultrices ante. Aliquam tincidunt, nisl at iaculis pulvinar, dolor nisl posuere est, consequat aliquam ante diam non sapien. Ut a venenatis diam. Curabitur sed turpis accumsan, vestibulum nunc imperdiet, condimentum purus. Donec a nunc vel libero congue semper consectetur et lorem.",
                Avatar = "http://demo.cmssuperheroes.com/themeforest/bookjunky/wp-content/uploads/team-3.jpg"
            }, new Author()
            {
                Id = 5,
                FullName = "Barbara Sleigh",
                Quote = "Nunc sed lacinia odio, eget ultrices ante. Aliquam tincidunt, nisl at iaculis pulvinar, dolor nisl posuere est, consequat aliquam ante diam non sapien. Ut a venenatis diam. Curabitur sed turpis accumsan, vestibulum nunc imperdiet, condimentum purus. Donec a nunc vel libero congue semper consectetur et lorem.",
                Avatar = "http://demo.cmssuperheroes.com/themeforest/bookjunky/wp-content/uploads/lady_author_4.jpg"
            });

            modelBuilder.Entity<Book>().HasData(new Book()
            {
                Id = 1,
                Name = "The Girl of Ink and Stars",
                Snippet = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat",
                Thumbnail = "/images/the_girl_of_ink_and_stars.jpg",
                Price = 30,
                Cover = "#081720",
                Overview = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                Published = new DateTime(),
                Hardcover = 2,
                Language = "English",
                Dimensions = new DateTime(),
                AuthorId = 1,
                AccountId = 1
            }, new Book()
            {
                Id = 2,
                Name = "Radical Gardening",
                Snippet = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat",
                Thumbnail = "/images/the_happy_lemon.jpg",
                Price = 20,
                Cover = "#332c28",
                Overview = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                Published = new DateTime(),
                Hardcover = 2,
                Language = "English",
                Dimensions = new DateTime(),
                AuthorId = 1,
                AccountId = 1
            }, new Book()
            {
                Id = 3,
                Name = "The Happy Lemon",
                Snippet = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat",
                Thumbnail = "/images/radical_gardening.jpg",
                Price = 35,
                Cover = "#ededa6",
                Overview = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                Published = new DateTime(),
                Hardcover = 2,
                Language = "English",
                Dimensions = new DateTime(),
                AuthorId = 1,
                AccountId = 1
            }, new Book()
            {
                Id = 4,
                Name = "Shattered",
                Snippet = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat",
                Thumbnail = "/images/shattered.jpg",
                Price = 25,
                Cover = "#bcbabb",
                Overview = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                Published = new DateTime(),
                Hardcover = 2,
                Language = "English",
                Dimensions = new DateTime(),
                AuthorId = 1,
                AccountId = 1
            }, new Book()
            {
                Id = 5,
                Name = "Freefall",
                Snippet = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat",
                Thumbnail = "/images/freefall.jpg",
                Price = 36,
                Cover = "#000000",
                Overview = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                Published = new DateTime(),
                Hardcover = 2,
                Language = "English",
                Dimensions = new DateTime(),
                AuthorId = 1,
                AccountId = 1
            }, new Book()
            {
                Id = 6,
                Name = "Darknet",
                Snippet = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat",
                Thumbnail = "/images/darknet.jpg",
                Price = 32,
                Cover = "#9f4949",
                Overview = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                Published = new DateTime(),
                Hardcover = 2,
                Language = "English",
                Dimensions = new DateTime(),
                AuthorId = 1,
                AccountId = 1
            }, new Book()
            {
                Id = 7,
                Name = "Boring Girls, A Novel",
                Snippet = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat",
                Thumbnail = "/images/boring_girls_a_novel.jpg",
                Price = 10,
                Cover = "#4027a7",
                Overview = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                Published = new DateTime(),
                Hardcover = 2,
                Language = "English",
                Dimensions = new DateTime(),
                AuthorId = 1,
                AccountId = 1
            }, new Book()
            {
                Id = 8,
                Name = "Clever Lands",
                Snippet = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat",
                Thumbnail = "/images/clever_lands.jpg",
                Price = 12,
                Cover = "#ff7536",
                Overview = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                Published = new DateTime(),
                Hardcover = 2,
                Language = "English",
                Dimensions = new DateTime(),
                AuthorId = 1,
                AccountId = 1
            }, new Book()
            {
                Id = 9,
                Name = "The Apocalypse of Lloyd",
                Snippet = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat",
                Thumbnail = "/images/lloyd.jpg",
                Price = 15,
                Cover = "#f4ed3c",
                Overview = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                Published = new DateTime(),
                Hardcover = 2,
                Language = "English",
                Dimensions = new DateTime(),
                AuthorId = 1,
                AccountId = 1
            }, new Book()
            {
                Id = 10,
                Name = "Red Queen",
                Snippet = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat",
                Thumbnail = "/images/red_queen.jpg",
                Price = 18,
                Cover = "#5aa6c8",
                Overview = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                Published = new DateTime(),
                Hardcover = 2,
                Language = "English",
                Dimensions = new DateTime(),
                AuthorId = 1,
                AccountId = 1
            }, new Book()
            {
                Id = 11,
                Name = "Principals of Agricultural Economics",
                Snippet = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat",
                Thumbnail = "/images/economic.jpg",
                Price = 22,
                Cover = "#5aa6c8",
                Overview = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                Published = new DateTime(),
                Hardcover = 2,
                Language = "English",
                Dimensions = new DateTime(),
                AuthorId = 1,
                AccountId = 1
            }, new Book()
            {
                Id = 12,
                Name = "Holy Ghosts",
                Snippet = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat",
                Thumbnail = "/images/holy_ghosts.jpg",
                Price = 22,
                Cover = "#180b06",
                Overview = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                Published = new DateTime(),
                Hardcover = 2,
                Language = "English",
                Dimensions = new DateTime(),
                AuthorId = 1,
                AccountId = 1
            });

            

            modelBuilder.Entity<Category>().HasData(
                new Category()
                {
                    Id = 1,
                    Name = "Business",
                },
                new Category()
                {
                    Id = 2,
                    Name = "Childrens",
                },
                new Category()
                {
                    Id = 3,
                    Name = "Comedy",
                },
                new Category()
                {
                    Id = 4,
                    Name = "Comic",
                },
                new Category()
                {
                    Id = 5,
                    Name = "Cooking",
                },
                new Category()
                {
                    Id = 6,
                    Name = "Fiction",
                },
                new Category()
                {
                    Id = 7,
                    Name = "Media",
                },
                new Category()
                {
                    Id = 8,
                    Name = "Romance",
                },
                new Category()
                {
                    Id = 9,
                    Name = "Science",
                },
                new Category()
                {
                    Id = 10,
                    Name = "Thriller",
                }
            );

            modelBuilder.Entity<BookCategory>().HasData(
                new BookCategory() {
                    Id = 1,
                    BookId = 1,
                    CategoryId = 1
                }, new BookCategory()
                {
                    Id = 2,
                    BookId = 1,
                    CategoryId = 2
                }, new BookCategory()
                {
                    Id = 3,
                    BookId = 1,
                    CategoryId = 5
                }, new BookCategory()
                {
                    Id = 4,
                    BookId = 2,
                    CategoryId = 5
                }, new BookCategory()
                {
                    Id = 5,
                    BookId = 3,
                    CategoryId = 5
                }, new BookCategory()
                {
                    Id = 6,
                    BookId = 4,
                    CategoryId = 5
                }, new BookCategory()
                {
                    Id = 7,
                    BookId = 5,
                    CategoryId = 5
                }, new BookCategory()
                {
                    Id = 8,
                    BookId = 6,
                    CategoryId = 5
                }

                );
        }

        public DbSet<CartClient> CartClient { get; set; }
    }
}
